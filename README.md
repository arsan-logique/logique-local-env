# Logique Local Environtment 
## Apache, PHP 7.2, PHP 5.6, MySQL 5.7, Java 8 OpenJDK

### Instruksi Instalasi

1. Clone repository dengan git
``` 
git clone https://gitlab.com/arsan-logique/logique-local-env.git 
```

2. Running file docker-compose.yml dan tunggu sampai selesai
``` 
docker-compose up -d 
```

3. Sebagai contoh, tambahkan atau buat direktori project misal : demo, dan tambahkan file index.php yang di dalamnya berisi kode di bawah ini
``` 
<?php phpinfo(); ?> 
```

4. Tambahkan Konfigurasi virtual hosts sesuai versi PHP dari project yang digunakan, misalnya menggunakan PHP 5.6. Tambahkan ke dalam file container/vhostsPHP5.6/default.conf 

``` 
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot "/var/www/html/demo"
    ServerName demo.local
	<Directory "/var/www/html/demo/">
		AllowOverride all
        Require all granted
	</Directory>
</VirtualHost> 
```
5. Tambahkan ServerName dari VirtualHost (contoh di atas demo.local) ke hosts file. Jika menggunakan linux tambahkan ke file /etc/hosts. contoh : 
``` 
127.0.0.1       demo.local
```

6. Buka browser lalu masukkan url http://demo.local. Jika halaman phpinfo tampil dengan baik maka instalasi Local Environtment berhasil